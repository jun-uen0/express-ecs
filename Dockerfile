FROM node:13-alpine

WORKDIR /Users/jun/Documents/dev/node/express-ec2

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000
CMD [ "node", "index.js" ]